## Status Aplikasi
[![Pipeline](https://gitlab.com/falyasekardina/story6-status/badges/master/pipeline.svg)](https://gitlab.com/falyasekardina/story6-status/commits/master)
[![Coverage](https://gitlab.com/falyasekardina/story6-status/badges/master/coverage.svg)](https://gitlab.com/falyasekardina/story6-status/commits/master)

## Link Heroku App
http://falya-ppw-status.herokuapp.com/
