from django import forms

class formStatus(forms.Form):
	status_attrs = {
        'type': 'text',
        'class': 'todo-form-input',
        'placeholder':'What is on your mind?',
    }
	status = forms.CharField(widget=forms.TextInput(attrs=status_attrs))
