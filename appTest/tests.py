from django.test import TestCase
from django.test import Client
from django.urls import resolve
from .views import index, profile
from .models import Status
from django.utils import timezone
from .forms import formStatus
# ---------
from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.chrome.options import Options
import unittest
import time
# Create your tests here.

class Lab6Test(TestCase):
	def test_url_is_exist(self):
		response = Client().get('/lab-6/')
		self.assertEqual(response.status_code, 200)

	def test_using_template(self):
		response = Client().get('/lab-6/')
		self.assertTemplateUsed(response, 'index.html')

	def test_using_func(self):
		found = resolve('/lab-6/')
		self.assertEqual(found.func, index)

	def test_model_can_create_new_message(self):
		#Creating a new activity
		new_activity = Status.objects.create(status='apa deh')
            
		#Retrieving all available activity
		counting_all_available_message= Status.objects.all().count()
		self.assertEqual(counting_all_available_message,1)

	def test_form_message_input_has_placeholder_and_css_classes(self):
		form = formStatus()
		self.assertIn('<p><label for="id_status">Status:</label> <input type="text" name="status" class="todo-form-input" placeholder="What is on your mind?" required id="id_status" /></p>', form.as_p())
	
	def test_lab6_post_success_and_render_the_result(self):
		message = 'Hello'
		response_post = Client().post('/lab-6/', {'message': message})
		self.assertEqual(response_post.status_code, 200)\

		response = Client().get('/lab-6/')
		html_response = response.content.decode('utf8')
		self.assertIn(message,html_response)

	def test_lab_6_showing_all_messages(self):
		status_saya = 'status'
		data_saya = {'message': status_saya}
		post_data_saya = Client().post('/lab-6/', data_saya)
		self.assertEqual(post_data_saya.status_code, 200)
	#--------------------------------------------------------
	def test_url_is_exist_challenge(self):
		response = Client().get('/profile-6/')
		self.assertEqual(response.status_code, 200)

	def test_using_template_challenge(self):
		response = Client().get('/profile-6/')
		self.assertTemplateUsed(response, 'profile.html')

	def test_using_func_challenge(self):
		found = resolve('/profile-6/')
		self.assertEqual(found.func, profile)

class FunctionalTest(unittest.TestCase):

	def setUp(self):
		chrome_options = Options()
		chrome_options.add_argument('--dns-prefetch-disable')
		chrome_options.add_argument('--no-sandbox')        
		chrome_options.add_argument('--headless')
		chrome_options.add_argument('disable-gpu')
		self.selenium  = webdriver.Chrome('./chromedriver', chrome_options=chrome_options)
		super(FunctionalTest, self).setUp()

	def test_can_write_status(self):
		selenium = self.selenium
		selenium.get('http://falya-ppw-status.herokuapp.com/')
		time.sleep(5)
		post = selenium.find_element_by_name('status')
		post.send_keys('Coba Coba')
		post.submit()
		time.sleep(8)
		self.assertIn("Coba", selenium.page_source)

	def test_posisi_layout1(self):
		selenium = self.selenium
		selenium.get('http://falya-ppw-status.herokuapp.com/')
		header_text = selenium.find_element_by_tag_name('p').text
		self.assertIn('Falya', header_text)

	def test_posisi_layout2(self):
		selenium = self.selenium
		selenium.get('http://falya-ppw-status.herokuapp.com/')
		header_text = selenium.find_element_by_tag_name('a').text
		self.assertIn('Profile', header_text)

	def test_css_class_insiderec(self):
		selenium = self.selenium
		selenium.get('http://falya-ppw-status.herokuapp.com/')
		clear_button = selenium.find_element_by_class_name('insiderec').value_of_css_property('padding')
		self.assertIn('30px', clear_button)

	def test_css_class_judul(self):
		selenium = self.selenium
		selenium.get('http://falya-ppw-status.herokuapp.com/')
		cek_class = selenium.find_element_by_class_name('judul').value_of_css_property('text-align')
		self.assertIn('center', cek_class)

if __name__ == '__main__' :
	unittest.main(warnings='ignore')		