from django.conf.urls import url
from .views import index, profile

urlpatterns = [
	url('', index, name='index'),
	url('/profile-6/', profile, name='profile'),
]