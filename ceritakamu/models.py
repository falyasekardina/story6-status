from django.db import models

# Create your models here.
class Cerita(models.Model):
	url = models.CharField(max_length=20, primary_key=True)
	judul = models.CharField(max_length=300)
	content = models.TextField(max_length=5000)
	image = models.CharField(max_length=100)
	created_date = models.DateTimeField(auto_now_add=True, editable=False, null=False, blank=False)