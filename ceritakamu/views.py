from django.shortcuts import render, redirect
from django.http import HttpResponseRedirect, HttpResponse
from django.template import RequestContext
from django.shortcuts import get_object_or_404
from .models import Cerita

# Create your views here.
response = {}

def index(request):
    cerita = Cerita.objects.all()
    response['cerita'] = cerita
    html = 'cerita.html'
    return render(request, html, response)

def detail(request, url):
    view = get_object_or_404(Cerita, pk=url)
    view.save()
    response['view'] = view
    html = 'detail.html'
    return render(request, html, response)

